# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.0] - 2024-05-23
### Added
- make it possible to pick the server with uci option

## [1.0.0] - 2024-05-23
### Changed
- build project using hatchling
- dependencies updates

## [0.1.0] - 2023-06-08
### Added
- initial version
