Foris Controller LibreSpeed module
==============================
This is a librespeed module for foris-controller

Requirements
============

* python3
* foris-controller

Installation
============

	``python3 setup.py install``
